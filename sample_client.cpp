#include "client_http.hpp"

typedef SimpleWeb::Client<SimpleWeb::HTTP> HttpClient;

int main() {
  HttpClient client("localhost:8080");

  auto response = client.request("GET", "/");

  std::cout << response->content.string() << std::endl;

  return 0;
}